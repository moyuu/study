<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------

$carFactory = new Factory();

$carFactory->createEngine();
$carFactory->createTyre();
