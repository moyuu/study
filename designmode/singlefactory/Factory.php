<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------

/**
 * 简单工厂
 */
class Factory
{
    public function createTyre($args = null): Tyre
    {
        return new Tyre($args);
    }

    public function createEngine($args = null): Engine
    {
        return new Engine($args);
    }
}
