<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------

/**
 * 对象池
 */
class Pool
{

    /**
     * 空闲的
     * @var array
     */
    protected $free = [];

    /**
     * 正在工作的
     * @var array
     */
    protected $working = [];

    public function get()
    {
        if (0 === count($this->free)) {
            $warter = new Warter;
        } else {
            $warter = array_pop($this->free);
        }
        $this->working[spl_object_hash($warter)] = $warter;
        return $warter;
    }

    public function recycle($instance)
    {
        $key = spl_object_hash($instance);

        if (isset($this->working[$key])) {
            unset($this->working[$key]);
        }

        $this->free[$key] = $instance;
    }

    public function count(): int
    {
        return count($this->free) + count($this->working);
    }
}
