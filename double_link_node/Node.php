<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------
class Node
{
    public $data;
    public $prev;
    public $next;

    public function __construct($data, $prev = null, $next = null)
    {
        $this->prev = $prev;
        $this->next = $next;
        $this->data = $data;
    }
}
