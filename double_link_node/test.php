<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------
include_once 'LinkList.php';
include_once 'Node.php';

$nodeList = new LinkList();

$nodeList->add(0, '1');
$nodeList->add(1, '2');
$nodeList->add(2, '3');
$nodeList->flip();
echo $nodeList->tostring();
