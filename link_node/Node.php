<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------
class Node
{
    public $data;
    public $next;

    public function __construct($value = null, $next = null)
    {
        $this->data = $value;
        $this->next = $next;
    }
}
