<?php
#+------------------------------------------------------------------
#| 普通的。
#+------------------------------------------------------------------
#| Author:Janmas Cromwell <janmas-cromwell@outlook.com>
#+------------------------------------------------------------------
include_once 'Node.php';
include_once 'NodeList.php';

$nodeList = new NodeList();
$nodeList->add(0, '曹操');
$nodeList->add(1, '刘备');
$nodeList->add(2, '周瑜');
$nodeList->add(3, '孔明');
$nodeList->add(4, '赵云');
$nodeList->add(5, '关羽');
$nodeList->add(6, '张飞');
$nodeList->push('aa');
sleep(1);
$new = $nodeList->flip();
echo ($nodeList->tostring()) . PHP_EOL;
